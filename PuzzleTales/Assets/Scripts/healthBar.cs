﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class healthBar : MonoBehaviour
{
    public GameObject healthBarHolder;
    
    public GameObject healthScaler;

    public enum HealthBarOptions
    { 
        AlwaysShow,
        OnlyShowWhenChanged,
    }
    public HealthBarOptions myStyle;

    public float TimeToFade = 5.0f;
    private float timer = 0.0f;

    float lastVal = 0.0f;
    bool firstRun = true;

    float originalScale; 

    // Start is called before the first frame update
    void Start()
    {
        Vector3 scale = healthScaler.transform.localScale;
        originalScale = scale.x;
    }

    public void updateHealthBar(float current, float max)
    {
        switch (myStyle)
        {
            case HealthBarOptions.AlwaysShow:
            {
                break;
            }
            case HealthBarOptions.OnlyShowWhenChanged:
                {
                    if (!firstRun)
                    {
                        if (lastVal != current)
                        {
                            healthBarHolder.SetActive(true);
                            healthScaler.SetActive(true);
                            transform.GetChild(2).gameObject.SetActive(true);
                            timer = 0.0f;
                        }
                        else if (timer < TimeToFade)
                        {
                            timer += Time.deltaTime;
                        }
                        else
                        {
                            healthBarHolder.SetActive(false);
                            healthScaler.SetActive(false);
                            transform.GetChild(2).gameObject.SetActive(false);
                        }
                    }
                    lastVal = current;
                    firstRun = false;
                    break;
                }
            default:break;
        }

        Vector3 scale = healthScaler.transform.localScale;
        scale.x = originalScale * (current / max);
        healthScaler.transform.localScale = scale;

        if (transform.gameObject.layer != 5) //If not in the UI layer
        {
            //Billboard
            transform.forward = transform.position - Camera.main.transform.position;
        }

    }


    // Update is called once per frame
    void Update()
    {
        
    }
}
