﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class mainMenuScene : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {

    }

    public void playGame()
    {
        SceneManager.LoadScene("Dungeon");
    }

    public void quitGame()
    {
        System.Diagnostics.Process.GetCurrentProcess().Kill();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
