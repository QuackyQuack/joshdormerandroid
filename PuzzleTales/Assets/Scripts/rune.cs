﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class rune : MonoBehaviour
{
    // Start is called before the first frame update
    public enum runeType
    {
        RUNE1,
        RUNE2,
        RUNE3,
        RUNE4,
        RUNE5,
        RUNE6,
    }

    public runeType myType;
    public gridSlot currentSlot;

    void Start()
    {


    }

    private void OnMouseDown()
    {
        //transform.localScale *= 1.1f;
        if (grid.runeInFormation(this))
        {
            grid.setFormationTracking(true,this);
        }
    }

    private void OnMouseUp()
    {
        //transform.localScale *= 1.1f;
        if (grid.runeInFormation(this))
        {
            grid.setFormationTracking(false, this);
        }
    }

    void Awake()
    {

    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
