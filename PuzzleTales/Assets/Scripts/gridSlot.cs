﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class gridSlot : MonoBehaviour
{

    public rune MyRune = null;
    public Vector2 myPosition = Vector2.zero;
    private Vector2 gridPosition = Vector2.zero;
    public void setRune(rune _rune)
    {
        if (_rune != null)
        {
            MyRune = _rune;
            MyRune.gameObject.transform.localPosition = myPosition;
            MyRune.currentSlot = this;
        }
        else
        {
            MyRune = null;
        }
    }

    static public void swapRunes(gridSlot source, gridSlot target)
    {
        rune sourceRune = source.MyRune;
        source.setRune(target.MyRune);
        target.setRune(sourceRune);
    }

    public void setGridLoc(int x, int y)
    {
        gridPosition = new Vector2(x, y);
    }

    public Vector2 getGridLoc()
    {
        return gridPosition;
    }
}
